# coding=utf-8
"""
this module should find occurencies of words and their position in a text
"""
import re
import operator


class WordsProcessor:
    """
    CountingWords class
    """

    chars_to_replace = {
        "é": "e",
        "è": "e",
        "à": "a",
        "ù": "u",
        "ç": "c",
        "ï": "i",
        "î": "i",
        "ô": "o",
        "œ": "oe",
    }
    chars_to_remove = {
        "-": "",
        "’": "",
        "'": "",
        " ": "",
    }
    chars_to_split_by = "[-’?!., ]"

    def replace_characters(self, word):
        """
        replaces a character with one in a predefined list
        checks each character in a word and if a match is found replaces it
        """
        replaced_word = ""

        for char in word:
            if char in self.chars_to_replace:
                replaced_word += self.chars_to_replace.get(char)
            else:
                replaced_word += char
        return replaced_word

    def remove_numbers(self, word):
        """
        check each word if it is a digit and return None if it is
        """
        if word.isdigit():
            return None
        else:
            return word

    def filter_empty_strings_from_list(self, words):
        """
        filter empty strings from list
        """
        return list(filter(None, words))

    def custom_split(self, text):
        """
        lower case string
        splits string where given character is found
        """
        return re.split(self.chars_to_split_by, text.lower())

    def sort_by_values_len(self, word_dict):
        """
        sort by value length
        """
        dict_len = {key: len(value) for key, value in word_dict.items()}
        sorted_key_list = sorted(
            dict_len.items(), key=operator.itemgetter(1), reverse=False,
        )
        sorted_dict = dict()
        for item in sorted_key_list:
            sorted_dict[item[0]] = word_dict[item[0]]
        return sorted_dict


def main():
    """
    main entry for the module
    """
    text = "La marche des vertueux est semée d’obstacles qui sont les entreprises\
 égoïstes que fait sans fin surgir l’œuvre du Malin. Béni soit-il l’homme de bonne volonté qui, au nom de la charité,\
 se fait le berger des faibles qu’il guide dans la vallée d’ombre, de la mort et des larmes, car il est le\
 gardien de son frère et la providence des enfants égarés. J’abattrai alors le bras d’une terrible colère,\
 d’une vengeance furieuse et effrayante sur les hordes impies qui pourchassent et réduisent à néant les brebis\
 de Dieu. Et tu connaîtras pourquoi mon nom est l’éternel quand sur toi s’abattra la vengeance du Tout-Puissant!\
 Ça fait des années que je répète ça. L’enfoiré qui l’entend, il meurt aussitôt. J’avais jamais cherché à\
 comprendre, je trouvais seulement que ça en jetait de dire ça avant de flinguer un mec. Et puis ce matin,\
 j’ai vu quelque chose qui m’a fait réfléchir. D’un seul coup, je me dis, ça pourrait bien vouloir dire que\
 tu es l’œuvre du malin, et que l’homme vertueux c’est moi, et que mon joli 9 mm ce serait mon protecteur,\
 mon berger dans la vallée de l’angoisse et des larmes. Ou encore mieux, c’est moi le berger et toi l’homme\
 vertueux, et c’est le monde qui est l’œuvre de Lucifer. Qu’est-ce que tu dis de ça ? Mais rien de tout ça\
 n’est juste. Ce qui est vrai, c’est que tu es le faible et que je suis la tyrannie des méchants. Et moi\
 j’essaie, Ringo, au prix d’un effort harassant, de protéger les faibles."

    words_processor = WordsProcessor()
    words_from_text = dict()
    words = list()

    for word in words_processor.custom_split(text):
        word = words_processor.replace_characters(word)
        word = words_processor.remove_numbers(word)
        words.append(word)
    words = words_processor.filter_empty_strings_from_list(words)

    for i, word in enumerate(words):
        if words_from_text.get(word):
            words_from_text[word].append(i)
        else:
            words_from_text[word] = [i]

    sorted_words = words_processor.sort_by_values_len(words_from_text)
    return sorted_words


if __name__ == "__main__":
    print(main())
